package com.example.expenser.data

import java.lang.ref.PhantomReference

class Expense{
    var referenceId:String?=null
    var category:String?=null
    var amount:Double?=null
    constructor(referenceId: String, category: String, amount: Double){
        this.referenceId=referenceId
        this.category=category
        this.amount=amount
    }
}