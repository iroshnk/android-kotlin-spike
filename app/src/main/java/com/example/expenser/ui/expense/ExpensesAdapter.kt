package com.example.expenser.ui.expense

import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.TextView
import com.example.expenser.data.Expense
import com.example.expenser.R

class ExpensesAdapter: BaseAdapter {
    var expenses = ArrayList<Expense>()
    var context: Context

    constructor(context: Context, expenses: ArrayList<Expense>):super(){
        this.expenses = expenses
        this.context = context
    }

    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {
        val expense = expenses[position]

        var inflator = context!!.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        var myView = inflator.inflate(R.layout.expense_grid_item, null)
        myView.findViewById<TextView>(R.id.referenceId).text = expense.referenceId
        myView.findViewById<TextView>(R.id.category).text = expense.category
        myView.findViewById<TextView>(R.id.amount).text = expense.amount.toString() + " LKR"

        myView.setOnClickListener{
            val intent = Intent(context, ExpenseDetails::class.java)
            intent.putExtra("referenceId",expense.referenceId)

            context.startActivity(intent)
        }

        return myView
    }

    override fun getItem(position: Int): Any {
        return expenses[position]
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    override fun getCount(): Int {
        return expenses.size
    }

}