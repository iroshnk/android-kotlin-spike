package com.example.expenser.ui.expense

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.TextView
import com.example.expenser.R

class ExpenseDetails : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_expense_details)

        val bundle:Bundle = intent.extras!!
        val referenceId = bundle.getString("referenceId")

        findViewById<TextView>(R.id.textView4).text = referenceId
    }
}