package com.example.expenser.ui.login

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast
import com.example.expenser.R
import com.example.expenser.data.LoginRepository
import com.example.expenser.ui.expense.ExpensesList

class MainActivity : AppCompatActivity() {
    private var loginRepository = LoginRepository()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        findViewById<Button>(R.id.login_button).setOnClickListener{
            val username = findViewById<EditText>(R.id.username)
            val password = findViewById<EditText>(R.id.password)

            if(loginRepository.login(username.text.toString(), password.text.toString())) {
                val intent = Intent(this, ExpensesList::class.java)
                //intent.putExtra("userName",username.text.toString())

                this.startActivity(intent)
            } else {
                Toast.makeText(this, "invalid username or pw",Toast.LENGTH_LONG).show()
            }
        }
    }
}