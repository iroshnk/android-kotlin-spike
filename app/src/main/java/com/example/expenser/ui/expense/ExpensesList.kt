package com.example.expenser.ui.expense

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.ListView
import com.example.expenser.data.Expense
import com.example.expenser.R
import com.example.expenser.data.ExpenseRepository

class ExpensesList : AppCompatActivity() {

    var expenseRepository = ExpenseRepository()
    var adapter: ExpensesAdapter?=null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_expenses_list)

        adapter = ExpensesAdapter(this, expenseRepository.getExpenses())
        var list = findViewById<ListView>(R.id.expenses_grid)
        list.adapter = adapter
    }
}